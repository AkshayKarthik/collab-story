Once upon a time...

As dawn broke over the quiet town, John Doe, the seasoned detective, was already up and about.
His keen eyes scanned the morning paper, looking for any hints of the mystery that had brought him here.
